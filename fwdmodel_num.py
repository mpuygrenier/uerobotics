from __future__ import print_function, division

import sympy

from fwdmodel_ref import f_kin_rad as f_kin_rad_sym
from fwdmodel_ref import f_kin_ref as f_kin_deg_sym

theta1 = sympy.Symbol('theta1')
theta2 = sympy.Symbol('theta2')
theta3 = sympy.Symbol('theta3')
f_kin_rad = sympy.lambdify((theta1, theta2, theta3), f_kin_rad_sym(theta1, theta2, theta3, symbolic=False))
f_kin_ref = sympy.lambdify((theta1, theta2, theta3), f_kin_deg_sym(theta1, theta2, theta3, symbolic=False))
