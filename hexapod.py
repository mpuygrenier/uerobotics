import time
import pydyn.dynamixel as dyn



def unlock_motors(ctrl):
	for motor in ctrl.motors:
		motor.torque_enable_raw = 0
		ctrl.synchronize()

def sleep_sync(ctrl,duree):
	ctrl.synchronize()
	time.sleep(duree)


def raz_pos(ctrl):
	ctrl.motors[ mid[33] ].goal_position_raw = 490
	ctrl.motors[ mid[23] ].goal_position_raw = 490
	ctrl.motors[ mid[13] ].goal_position_raw = 490
	ctrl.motors[ mid[63] ].goal_position_raw = 490
	ctrl.motors[ mid[53] ].goal_position_raw = 490
	ctrl.motors[ mid[43] ].goal_position_raw = 490

	# sleep_sync(ctrl,1)

	ctrl.motors[ mid[22] ].goal_position_raw = 535#
	ctrl.motors[ mid[12] ].goal_position_raw = 535#
	ctrl.motors[ mid[62] ].goal_position_raw = 535#
	ctrl.motors[ mid[52] ].goal_position_raw = 535#
	ctrl.motors[ mid[42] ].goal_position_raw = 535#
	ctrl.motors[ mid[32] ].goal_position_raw = 535#

	sleep_sync(ctrl,0.7)

	#leve les pates 1
	ctrl.motors[ mid[12] ].goal_position_raw = 400
	ctrl.motors[ mid[52] ].goal_position_raw = 400
	ctrl.motors[ mid[32] ].goal_position_raw = 400

	sleep_sync(ctrl,0.7)

	#reset pivot pates 1
	ctrl.motors[ mid[11] ].goal_position_raw = 300
	ctrl.motors[ mid[51] ].goal_position_raw = 515
	ctrl.motors[ mid[31] ].goal_position_raw = 730

	sleep_sync(ctrl,0.7)

	#pose pates 1
	ctrl.motors[ mid[12] ].goal_position_raw = 535
	ctrl.motors[ mid[52] ].goal_position_raw = 535
	ctrl.motors[ mid[32] ].goal_position_raw = 535

	sleep_sync(ctrl,0.7)

	#leve pates 2
	ctrl.motors[ mid[22] ].goal_position_raw =400
	ctrl.motors[ mid[62] ].goal_position_raw =400
	ctrl.motors[ mid[42] ].goal_position_raw = 400

	sleep_sync(ctrl,0.7)

	#reset pivot pates 2
	ctrl.motors[ mid[21] ].goal_position_raw =515
	ctrl.motors[ mid[61] ].goal_position_raw = 730
	ctrl.motors[ mid[41] ].goal_position_raw = 300


	sleep_sync(ctrl,0.7)

	#pose pates 2
	ctrl.motors[ mid[22] ].goal_position_raw =535
	ctrl.motors[ mid[62] ].goal_position_raw =535
	ctrl.motors[ mid[42] ].goal_position_raw = 535

	sleep_sync(ctrl,0.7)

def walk_oneMeter(ctrl):
	ctrl.motors[ mid[12] ].goal_position_raw = 400
	ctrl.motors[ mid[52] ].goal_position_raw = 400
	ctrl.motors[ mid[32] ].goal_position_raw = 400
	
	cpt=0
	while cpt<14:
		cpt+=1
		#pivote pates 1
		ctrl.motors[ mid[11] ].goal_position_raw = 200
		ctrl.motors[ mid[51] ].goal_position_raw = 615
		ctrl.motors[ mid[31] ].goal_position_raw = 630


		sleep_sync(ctrl,0.1)

		#pose pates 1
		ctrl.motors[ mid[12] ].goal_position_raw = 535
		ctrl.motors[ mid[52] ].goal_position_raw = 535
		ctrl.motors[ mid[32] ].goal_position_raw = 535

		sleep_sync(ctrl,0.1)

		#leve pates 2
		ctrl.motors[ mid[22] ].goal_position_raw =400
		ctrl.motors[ mid[62] ].goal_position_raw =400
		ctrl.motors[ mid[42] ].goal_position_raw = 400

		#reset pivot pates 1
		ctrl.motors[ mid[11] ].goal_position_raw = 300
		ctrl.motors[ mid[51] ].goal_position_raw = 515
		ctrl.motors[ mid[31] ].goal_position_raw = 730


		sleep_sync(ctrl,0.1)

		#pivot pates 2
		ctrl.motors[ mid[21] ].goal_position_raw =410
		ctrl.motors[ mid[61] ].goal_position_raw = 830
		ctrl.motors[ mid[41] ].goal_position_raw = 400

		sleep_sync(ctrl,0.1)

		#pose pates 2
		ctrl.motors[ mid[22] ].goal_position_raw =535
		ctrl.motors[ mid[62] ].goal_position_raw =535
		ctrl.motors[ mid[42] ].goal_position_raw = 535

		sleep_sync(ctrl,0.1)

		#leve les pates 1
		ctrl.motors[ mid[12] ].goal_position_raw = 400
		ctrl.motors[ mid[52] ].goal_position_raw = 400
		ctrl.motors[ mid[32] ].goal_position_raw = 400

		#reset pivot pates 2
		ctrl.motors[ mid[21] ].goal_position_raw =515
		ctrl.motors[ mid[61] ].goal_position_raw = 730
		ctrl.motors[ mid[41] ].goal_position_raw = 300

		sleep_sync(ctrl,0.1)

	#pose pates 1
	ctrl.motors[ mid[12] ].goal_position_raw = 535
	ctrl.motors[ mid[52] ].goal_position_raw = 535
	ctrl.motors[ mid[32] ].goal_position_raw = 535

	sleep_sync(ctrl,0.1)


def turn_90_degrees(ctrl):
	cpt=0
	while cpt < 6:
		cpt+=1
		#leve les pates 1
		ctrl.motors[ mid[12] ].goal_position_raw = 400
		ctrl.motors[ mid[52] ].goal_position_raw = 400
		ctrl.motors[ mid[32] ].goal_position_raw = 400
		sleep_sync(ctrl,0.1)

		#pivote pates 1
		ctrl.motors[ mid[11] ].goal_position_raw = 200
		ctrl.motors[ mid[51] ].goal_position_raw = 415
		ctrl.motors[ mid[31] ].goal_position_raw = 630
		sleep_sync(ctrl,0.1)
			
		#pose pates 1
		ctrl.motors[ mid[12] ].goal_position_raw = 535
		ctrl.motors[ mid[52] ].goal_position_raw = 535
		ctrl.motors[ mid[32] ].goal_position_raw = 535
		sleep_sync(ctrl,0.1)

		#leve pates 2
		ctrl.motors[ mid[22] ].goal_position_raw = 400
		ctrl.motors[ mid[62] ].goal_position_raw = 400
		ctrl.motors[ mid[42] ].goal_position_raw = 400
		sleep_sync(ctrl,0.1)

		#reset pivot pattes 1
		ctrl.motors[ mid[11] ].goal_position_raw = 300
		ctrl.motors[ mid[51] ].goal_position_raw = 515
		ctrl.motors[ mid[31] ].goal_position_raw = 730
		sleep_sync(ctrl,0.1)

		#pose pates 2
		ctrl.motors[ mid[22] ].goal_position_raw = 535
		ctrl.motors[ mid[62] ].goal_position_raw = 535
		ctrl.motors[ mid[42] ].goal_position_raw = 535
		sleep_sync(ctrl,0.1)


def walk_turn_walk(ctrl):
	walk_oneMeter(ctrl)
	turn_90_degrees(ctrl)
	walk_oneMeter(ctrl)

def walk_2meters(ctrl):
	walk_oneMeter(ctrl)
	time.sleep(1)
	walk_oneMeter(ctrl)

def turn_360(ctrl):
	turn_90_degrees(ctrl)
	time.sleep(0.1)
	turn_90_degrees(ctrl)
	time.sleep(0.1)
	turn_90_degrees(ctrl)
	time.sleep(0.1)
	turn_90_degrees(ctrl)

ctrl = dyn.create_controller(baudrate=1000000, verbose=True, start=True)
time.sleep(1)

it=0
mid={}
for motor in ctrl.motors:
	mid[motor.id]=it
	it+=1
	print motor.id
	motor.torque_enable_raw = 1
	motor.moving_speed_raw = 150
print mid


raz_pos(ctrl)
sleep_sync(ctrl,0.2)
# walk_turn_walk(ctrl)
turn_360(ctrl)
# walk_2meters(ctrl)

ctrl.close()


