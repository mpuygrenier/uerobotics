from __future__ import print_function, division
import random
import time

from fwdmodel_num import f_kin_ref


n = 200000
start = time.time()
for _ in range(n):
    f_kin_ref(random.uniform(-180, 180), random.uniform(-180, 180), random.uniform(-180, 180))

print('{} f_kin done in {:.2f}s'.format(n, time.time()-start))